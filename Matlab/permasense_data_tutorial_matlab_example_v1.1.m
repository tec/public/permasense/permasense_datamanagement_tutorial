% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% %
% % PermaSense Data Manangement - Tutorial
% % This is an example for downloading data from data.permasense.ch
% %
% % @author:    Samuel Weber, Christoph Walser
% % @date:      April 15, 2014
% % @version:   v1.1
% %
% %
% %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %



%% DEFINE DIFFERENT PATHS AND VARIABLES (CHANGE TO YOUR NEEDS)
% Path on local computer to save data:
path_data_local='Data/';
% File name for the saved data:
name_data_local='DH_gps_batch_daily.csv';

% Time limits of data to fetch:
date_beg='01/01/2014';
date_end='07/04/2016';

disp('All variables are set.')



%% DOWNLOAD DATA FROM DATA.PERMASENSE.CH
if (exist(path_data_local, 'dir') ~=7)
    mkdir (path_data_local)
    disp(strcat('Directory',32,path_data_local, 32,'is created'))
end

addpath (path_data_local)
addpath ../

disp('Downloading GPS Batch data...')
url_download=strcat('http://data.permasense.ch/multidata?vs[0]=dirruhorn_gps_batch&time_format=iso&field[0]=All&from=',date_beg,'+00:00:00&to=',date_end,'+00:00:00');
csv_savepath=strcat(path_data_local, '/', name_data_local);
urlwrite(url_download,csv_savepath);
disp('Crackmeter (tctc) data has been downloaded.')



%% IMPORT DATA
disp('Importing crackmeter (tctc) data...')
% Import CSV file containing Matterhorn crackmeter data, exported from the GSN interface
% (simply assume we have exported all fields):
[position,	device_id,	generation_time,	sensortype,...
 sensortype_serialid,  header_seqnr, header_originatorid, header_atime,...
 payload_sample_valid, payload_sample_no, tctc_dx0, tctc_t1, tctc_dx1, tctc_t2,...
 timed ]= textread(csv_savepath,'%d %d %f %*d %s %d %d %d %f %d %d %f %f %f %f %s',...
 'headerlines',3,'delimiter',',','whitespace','null','emptyvalue',NaN);

% Get a list of indices without duplicates, sorted by ascending time:
[t_temp,noduplicate]=unique(generation_time);

% Apply these indices to the imported data:
cr.pos=position(noduplicate);
cr.t1=tctc_t1(noduplicate); cr.t2=tctc_t2(noduplicate);
cr.dx0=tctc_dx0(noduplicate); cr.dx1=tctc_dx1(noduplicate);

% Time conversion from time used in GSN (UTC) to Zurich time (CEST):
cr.time=datenum(1970,1,1) +generation_time(noduplicate)/(3600*24*1000)...
    +datenum(0,0,0,2,0,0);

% Clean up workspace:
clearvars -except path_data_local date_beg date_end cr
disp('Crackmeter (tctc) data has been imported.')
disp('The structure contains the fields: ')
disp(cr)



%% PLOT DATA
disp('Plotting data...')
plot(cr.time, cr.t1)

% Measurement gaps in the raw data cause Matlab to plot straight lines.
% By inserting NaN values for gaps bigger then 2 days (using index 'k'),
% the graph can be plotted more nicely:
k= floor(diff(cr.time))>2;
cr.time(k)=NaN;
figure;
plot(cr.time, cr.t1);

disp('Plotted data.')

disp('Done. Go have an ice cream...')
