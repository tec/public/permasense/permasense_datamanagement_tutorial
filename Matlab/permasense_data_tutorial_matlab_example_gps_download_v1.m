% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % 
% % PermaSense Data Manangement - Tutorial
% % This is an example for downloading data from data.permasense.ch
% % 
% % @author:    Samuel Weber, Christoph Walser, Jan Beutel
% % @date:      April 08, 2016
% % @version:   v1
% % 
% % 
% % 
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %



%% DEFINE DIFFERENT PATHS AND VARIABLES (CHANGE TO YOUR NEEDS)
% Path on local computer to save data:
path_data_local='Data/';

% Time limits of data to fetch:
date_beg='16/08/2014';
date_end='05/05/2016';

% Deployment Name
%deployment='dirruhorn';
deployment='matterhorn';
%deployment='permmos';
%deployment='saastal';

% Position to be fetched
position=40; %64, 83, 84

% File name for the saved data:
name_data_local=strcat(deployment, '_gps_batch_daily_position_', num2str(position), '.csv');

disp('All variables are set.')



%% DOWNLOAD DATA FROM DATA.PERMASENSE.CH
if (exist(path_data_local, 'dir') ~=7)
    mkdir (path_data_local)
    disp(strcat('Directory',32,path_data_local, 32,'is created'))
end

addpath (path_data_local)
addpath ../

disp('Downloading GPS Batch data...');
disp(strcat('for deployment: ',32,deployment,32,'from',32,date_beg,32,'to',32,date_end));
url_download=strcat('http://data.permasense.ch/multidata?vs[0]=',deployment,'_gps_differential__batch__daily&time_format=iso&field[0]=All&c_join[0]=and&c_vs[0]=',deployment,'_gps_differential__batch__daily&c_field[0]=position&c_min[0]=',num2str(position-1),'&c_max[0]=',num2str(position),'&from=',date_beg,'+00:00:00&to=',date_end,'+00:00:00');
csv_savepath=strcat(path_data_local, '/', name_data_local);
urlwrite(url_download,csv_savepath); 
disp('GPS Batch data has been downloaded.')

